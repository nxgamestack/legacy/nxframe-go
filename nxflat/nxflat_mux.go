package nxflat

import (
	"bytes"
	"fmt"

	"gitlab.com/nxgamestack/nxframe/nxframe-go/nxwrap"
)

type Multiplexer struct {
	meta_buffer map[uint][]byte
	buffer      []byte

	initialized bool
}

func (m *Multiplexer) init() {
	if !m.initialized {
		m.meta_buffer = make(map[uint][]byte)
	}
}

func (m *Multiplexer) labelBuffer(programID string, messageKind string, channelID uint) {
	m.meta_buffer[channelID] = append(m.meta_buffer[channelID], []byte(fmt.Sprintf("%s %s %v ", programID, messageKind, channelID))...)
}

func (m *Multiplexer) Push(programID string, messageKind string, channelID uint, data []byte) {
	m.init()

	if len(m.meta_buffer[channelID]) == 0 {
		// m.meta_buffer[channelID] = append(m.meta_buffer[channelID], []byte(fmt.Sprintf("%s %s %v ", programID, messageKind, channelID))...)
		m.labelBuffer(programID, messageKind, channelID)
	}
	if bytes.ContainsAny(data, "\n\r") {
		first := bytes.IndexAny(data, "\n\r")

		tmpLine := append(m.meta_buffer[channelID], data[0:first]...)
		tmpLine = bytes.TrimSpace(tmpLine)
		segs := bytes.Split(tmpLine, []byte(" "))
		//log.Printf("%v\n", segs[1])
		if string(segs[1]) != nxwrap.KindData || len(segs) > 3 {
			m.buffer = append(m.buffer, tmpLine...)
			m.buffer = append(m.buffer, []byte("\n")...)
		}

		m.meta_buffer[channelID] = []byte("")
		m.Push(programID, messageKind, channelID, data[first+1:])

	} else if messageKind != nxwrap.KindData {
		m.meta_buffer[channelID] = []byte("")
		m.labelBuffer(programID, messageKind, channelID)

		m.buffer = append(m.buffer, m.meta_buffer[channelID]...)
		m.buffer = append(m.buffer, data...)
		m.buffer = append(m.buffer, []byte("\n")...)

		m.meta_buffer[channelID] = []byte("")
	} else {
		m.meta_buffer[channelID] = append(m.meta_buffer[channelID], data...)
	}
}

func (n *Multiplexer) PushMessage(message nxwrap.Message) {
	n.Push(message.SourceID, message.Kind, message.ChannelID, message.Data)
}

func (m *Multiplexer) Peek() []byte {
	return m.buffer
}

func (m *Multiplexer) Pop() []byte {
	r := m.Peek()
	m.buffer = []byte("")
	return r
}
