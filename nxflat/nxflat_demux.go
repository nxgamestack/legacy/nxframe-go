package nxflat

import (
	"bytes"
	"log"
	"strconv"

	bp "github.com/nexustix/boilerplate"
	"gitlab.com/nxgamestack/nxframe/nxframe-go/nxwrap"
)

type Demultiplexer struct {
	buffer    []byte
	msgBuffer []nxwrap.Message
}

func (m *Demultiplexer) convert(octets []byte) nxwrap.Message {
	message := nxwrap.Message{}
	internalError := nxwrap.Message{}
	internalError.SourceID = nxwrap.KindInternalError
	internalError.Kind = nxwrap.KindInternalError
	internalError.ChannelID = 0
	internalError.Data = []byte("")

	segs := bytes.SplitN(octets, []byte(" "), 4)
	if len(segs) < 4 {
		log.Printf("<-> FAIL read message has insufficient length")
		return internalError
	}
	message.SourceID = string(segs[0])
	message.Kind = string(segs[1])
	ChannelID, err := strconv.ParseUint(string(segs[2]), 10, 32)
	if bp.GotError(err) {
		log.Printf("<-> CRITICAL failed to decode client ID >%s<", err)
		return internalError
	}
	message.ChannelID = uint(ChannelID)
	// let nxflat message imply newline at end of message
	// TODO make that an option - escape codes ? - as flag ?
	message.Data = append(segs[3], []byte("\n")...)
	return message
}

func (m *Demultiplexer) Push(octets []byte) {
	if bytes.ContainsAny(octets, "\n\r") {
		first := bytes.IndexAny(octets, "\n\r")
		m.buffer = append(m.buffer, append(m.buffer, octets[0:first]...)...)
		m.msgBuffer = append(m.msgBuffer, m.convert(m.buffer))
		m.buffer = []byte("")
		m.Push(octets[first+1:])
	} else {
		m.buffer = append(m.buffer, octets...)
	}
}

func (m *Demultiplexer) Peek() (nxwrap.Message, bool) {
	if len(m.msgBuffer) >= 1 {
		return m.msgBuffer[0], true
	}
	return nxwrap.Message{}, false
}

func (m *Demultiplexer) Pop() (nxwrap.Message, bool) {
	if len(m.msgBuffer) >= 1 {
		r := m.msgBuffer[0]
		m.msgBuffer = m.msgBuffer[1:]
		return r, true
	}
	return nxwrap.Message{}, false
}
