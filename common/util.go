package common

func ByteEqAt(data []byte, index int, value byte) bool {
	if index >= 0 && index < len(data) {
		return data[index] == value
	}
	return false
}

/*
func FindByte(data []byte, search byte, offset int, escapable bool) int {
	i := offset

	for i < len(data) {
		if ByteEqAt(data, i, search) {
			if ByteEqAt(data, i+1, search) {
				i = i + 1
			} else {
				return i
			}
		}
		i = i + 1
	}
	return -1
}
*/

func FindByteEsc(octets []byte, search byte, startIndex int, escaped bool) int {
	i := startIndex

	for i < len(octets) {
		if ByteEqAt(octets, i, search) {
			if escaped {
				// check bounds so characters at end of bytes
				// aren't seen as unescaped by default
				if i+1 < len(octets) {
					if ByteEqAt(octets, i+1, search) {
						i = i + 1
					} else {
						return i
					}
				} else {
					return -1
				}
			} else {
				return i
			}
		}
		i = i + 1
	}
	return -1
}
