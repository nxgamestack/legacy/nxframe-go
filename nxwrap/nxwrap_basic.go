package nxwrap

import (
	"bytes"
	"fmt"
	"log"
	"strconv"

	bp "github.com/nexustix/boilerplate"
	"gitlab.com/nxgamestack/nxframe/nxframe-go/common"
)

const (
	KindData          string = "dat"
	KindJoin          string = "new"
	KindLeave         string = "gon"
	KindError         string = "err"
	KindInternalError string = "<!>"
)

func Wrap(programID string, messageKind string, channelID uint, data []byte) []byte {
	var buff []byte
	var dataEscaped []byte

	channelIDString := fmt.Sprint(channelID)

	dataEscaped = bytes.ReplaceAll(data, []byte(")"), []byte("))"))
	dataEscaped = bytes.ReplaceAll(dataEscaped, []byte("("), []byte("(("))

	buff = append(buff, '(')
	buff = append(buff, []byte(programID)...)
	buff = append(buff, ' ')
	buff = append(buff, []byte(messageKind)...)
	buff = append(buff, ' ')
	buff = append(buff, []byte(channelIDString)...)
	buff = append(buff, ' ')
	buff = append(buff, dataEscaped...)
	buff = append(buff, ')')
	buff = append(buff, ' ')

	return buff

}

func Unwrap(octets []byte, frameStart, frameEnd int) (string, string, uint, []byte) {
	segs := bytes.SplitN(octets[frameStart+1:frameEnd], []byte(" "), 4)
	if len(segs) < 4 {
		log.Printf("<-> FAIL read message has insufficient length")
		return KindInternalError, KindInternalError, 0, []byte("")
	}
	programID := string(segs[0])
	messageKind := string(segs[1])
	ChannelID, err := strconv.ParseUint(string(segs[2]), 10, 32)
	if bp.GotError(err) {
		log.Printf("<-> CRITICAL failed to decode client ID >%s<", err)
		return KindInternalError, KindInternalError, 0, []byte("")
	}
	data := bytes.ReplaceAll(segs[3], []byte("))"), []byte(")"))
	data = bytes.ReplaceAll(data, []byte("(("), []byte("("))
	return programID, messageKind, uint(ChannelID), data

}

func FindFrame(octets []byte) ([2]int, bool) {
	var location [2]int
	location[0] = common.FindByteEsc(octets, '(', 0, true)
	if location[0] > -1 {
		location[1] = common.FindByteEsc(octets, ')', location[0], true)
		if location[1] > -1 {
			return location, true
		}
	}
	return location, false
}
