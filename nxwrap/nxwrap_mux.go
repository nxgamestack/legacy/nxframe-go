package nxwrap

type Multiplexer struct {
	buffer []byte
}

func (n *Multiplexer) Push(programID string, messageKind string, channelID uint, data []byte) {
	n.buffer = append(n.buffer, Wrap(programID, messageKind, channelID, data)...)
}

func (n *Multiplexer) PushMessage(message Message) {
	n.Push(message.SourceID, message.Kind, message.ChannelID, message.Data)
}

func (n *Multiplexer) Peek() []byte {
	return n.buffer
}

func (n *Multiplexer) Pop() []byte {
	r := n.buffer
	n.buffer = []byte("")
	return r
}
