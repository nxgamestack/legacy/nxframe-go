package nxwrap

type Message struct {
	SourceID  string
	Kind      string
	ChannelID uint
	Data      []byte
}
