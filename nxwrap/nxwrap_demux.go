package nxwrap

type Demultiplexer struct {
	buffer []byte
}

func (n *Demultiplexer) Push(octets []byte) {
	n.buffer = append(n.buffer, octets...)
}

func (n *Demultiplexer) Peek() (Message, bool) {
	location, success := FindFrame(n.buffer)
	if success {
		programID, messageKind, ChannelID, data := Unwrap(n.buffer, location[0], location[1])
		if messageKind != KindInternalError {
			return Message{programID, messageKind, ChannelID, data}, true
		}
	}
	return Message{}, false
}

func (n *Demultiplexer) Pop() (Message, bool) {
	location, success := FindFrame(n.buffer)
	if success {
		programID, messageKind, ChannelID, data := Unwrap(n.buffer, location[0], location[1])
		n.buffer = n.buffer[location[1]+1:]
		if messageKind != KindInternalError {
			return Message{programID, messageKind, ChannelID, data}, true
		}
	}
	_, success = FindFrame(n.buffer)
	if success {
		return n.Pop()
	}
	return Message{}, false
}
